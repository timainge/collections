# MongoDB Browser Example App

Example Sinatra application to browser collections and documents in a MongoDB.

Supports the following:

* Create collections
* Drop empty collections
* Create document in a collection
* Edit document 
* Delete document
* Validates JSON in browser before posting document add or update

